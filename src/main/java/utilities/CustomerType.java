package utilities;

/**
 * Author: Jannik Hausin
 */
public enum CustomerType {
    USUAL, BIKE, DISABLED, WOMEN, LOCAL
}
